import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import numpy as np


def umatrix(model, epoch_data, prefix=''):
    distances = np.zeros(model.W.shape[:-1])
    
    for i in range(len(distances)):
        for j in range(len(distances[0])):
            temp = []
            if i != 0:
                temp.append((model.W[i][j] - model.W[i-1][j])**2)
            if j != 0:
                temp.append((model.W[i][j] - model.W[i][j-1])**2)
            if i != len(distances)-1:
                temp.append((model.W[i][j] - model.W[i+1][j])**2)
            if j != len(distances[0])-1:
                temp.append((model.W[i][j] - model.W[i][j+1])**2)

            distances[i][j] = np.mean(temp)

    plt.imshow(distances, interpolation='none', norm=LogNorm())
    plt.title('inter-node average distances, umatrix')
    plt.xlabel('column')
    plt.ylabel('row')
    plt.colorbar()
    plt.savefig(prefix + '_' + str(epoch_data['epoch']) + '_distance.png')
    plt.clf()

def count(model, data, prefix=''):

    counts = np.zeros(model.W.shape[:-1])
    for i in range(len(data)):
        loc = model.MapEvent(data[i])
        counts[loc] += 1

    plt.imshow(counts, interpolation='none', norm=LogNorm())
    plt.title('Counting mapped data')
    plt.xlabel('column')
    plt.ylabel('row')
    plt.colorbar()
    plt.show()
    plt.savefig(prefix + '_counts.png')
    plt.clf()

def showone(model, loc, prefix=''):

    logging.info('in showone')

    plt.plot(model.W[loc])
    plt.title('Map Element %s' % str(loc))
    plt.xlabel('Element index')
    plt.ylabel('Magnitude')
    plt.savefig(prefix + '_%i_%i_showone.png' % loc)
    plt.clf()
