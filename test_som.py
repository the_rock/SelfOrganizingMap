import som
import visualize
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LogNorm
import logging

# Three well spereated normal distributions
def test_1():

    # Generate a gaussian at (10,0,10)
    X = np.zeros((1000,3))
    X[:,0] = np.random.normal(10.0, 1.0, 1000)
    X[:,1] = np.random.normal(0.0, 1.0, 1000)
    X[:,2] = np.random.normal(10.0, 1.0, 1000)
    # Generate a gaussian at (0,10,10)
    Y = np.zeros((1000,3))
    Y[:,0] = np.random.normal(0.0, 1.0, 1000)
    Y[:,1] = np.random.normal(10.0, 1.0, 1000)
    Y[:,2] = np.random.normal(10.0, 1.0, 1000)
    # Generate a gaussian at (10,10,0)
    W = np.zeros((1000,3))
    W[:,0] = np.random.normal(10.0, 1.0, 1000)
    W[:,1] = np.random.normal(10.0, 1.0, 1000)
    W[:,2] = np.random.normal(0.0, 1.0, 1000)

    # COmbine the data
    Z = np.concatenate((X,Y,W))

    # Run the self organizing map
    logging.basicConfig(filename='train.log', level=logging.DEBUG)
    model = som.T_SOM(log_obj=logging.getLogger('model'), shape=(20,20,3))
    model.Train(Z,n_epochs=100,f=visualize.umatrix,f_arg=('test1',))

# A box within a hollow box, each uniformly filled
def test_2():

    # Generate inner box
    X = np.zeros((1000,3))
    X[:,0] = np.random.uniform(-10.0, 10.0, 1000)
    X[:,1] = np.random.uniform(-10.0, 10.0, 1000)
    X[:,2] = np.random.uniform(-10.0, 10.0, 1000)

    # Generate larger box
    Y = np.zeros((10000,3))
    Y[:,0] = np.random.uniform(-30.0, 30.0, 10000)
    Y[:,1] = np.random.uniform(-30.0, 30.0, 10000)
    Y[:,2] = np.random.uniform(-30.0, 30.0, 10000)
    Z = []

    # Hollow out the box
    for y in Y:
        if abs(y[0]) < 20 and abs(y[1]) < 20 and abs(y[2]) < 20:
            continue
        Z.append(y)
    Y = np.array(Z)

    # Combine the data
    Z = np.concatenate((X,Y))

    # Run the self organizing map
    logging.basicConfig(filename='train.log', level=logging.DEBUG)
    model = som.T_SOM(log_obj=logging.getLogger('model'), shape=(50,50,3))
    model.Train(Z,n_epochs=100,f=visualize.umatrix,f_arg=('test2',))


# A gaussian sphere inside a hemispherical shell
def test_3():

    # Generate hemisphere
    X = np.zeros((1000,3))
    X[:,0] = np.random.uniform(-10.0, 10.0, 1000)
    temp = list(np.sqrt(100 - x[0]**2) for x in X)
    X[:,1] = np.array(list(np.random.uniform(-t, t, 1)[0] for t in temp))
    X[:,2] = np.array(list(np.sqrt(100 - x[0]**2 - x[1]**2) for x in X))

    if np.any(np.isnan(X)):
        print 'Had an error generating the data to test with, please re-run'
        return

    # Generate a gaussian around the origin
    Y = np.zeros((1000,3))
    Y[:,0] = np.random.normal(0.0, 1.0, 1000)
    Y[:,1] = np.random.normal(0.0, 1.0, 1000)
    Y[:,2] = np.random.normal(0.0, 1.0, 1000)

    # Combine data sets into one
    Z = np.concatenate((X,Y))

    # Run the self organizing map
    logging.basicConfig(filename='train.log', level=logging.DEBUG)
    model = som.T_SOM(log_obj=logging.getLogger('model'), shape=(20,20,3))
    model.Train(Z,n_epochs=100,f=visualize.umatrix,f_arg=('test3',))



