import theano
import theano.tensor as T
import numpy as np
import pickle as pkl
#import visualize
import logging
from time import time

class SOM(object):

    def __init__(self, shape=None, low=0.0, high=1.0, W=None, log_obj=None, loadfile=None):

        if W is not None:
            self.W = W
        elif shape is not None:
            self.W = np.random.uniform(low=low, high=high, size=shape)
        elif loadfile is not None:
            self.W = self.__Load(loadfile)
        else:
            raise ValueError('Nothing to initialize SOM with.')

        if log_obj is not None:
            self.log_obj = log_obj
            self.log_obj.info('W has shape: %s' % str(self.W.shape))
        else:
            self.log_obj = None
            
        self.mgrid = np.meshgrid(*list(range(self.W.shape[i]) for i in range(len(self.W.shape) - 1)))
        
    def MapEvent(self, x):
        
        activations = np.sum((self.W - x)**2, axis=len(self.W.shape) - 1)
        return np.unravel_index(activations.argmin(),activations.shape)
    
    def Activations(self, x):

        return np.sum((self.W - x)**2, axis=len(self.W.shape) - 1)

    def WinnerLoc(self, A):

        return np.unravel_index(A.argmin(),A.shape)

    def Rates(self, minloc, neighbourhood):
        
        return np.exp(-reduce(lambda x,y:x+y,list((self.mgrid[i-1] - minloc[i])**2 for i in range(len(minloc))))/neighbourhood).reshape(list(self.W.shape[:-1]) + [1])
        
    def Train(self, X, n_epochs=500, alpha_init=1.0, alpha_fin=1e-2, neighbourhood_init=-1, neighbourhood_fin=2.0, stopping_tol = 0.0, f = None, f_arg = tuple()):

        if neighbourhood_init == -1:
            neighbourhood_init = max(self.W.shape[:-1])

        alpha_shrink = float(alpha_init - alpha_fin) / (alpha_fin * n_epochs)
        neighbourhood_shrink = float(neighbourhood_init - neighbourhood_fin) / (neighbourhood_fin * n_epochs)

        if self.log_obj is not None:
            self.log_obj.info('Data size: %i' % len(X))
            self.log_obj.info('Element size: %i' % len(X[0]))
            self.log_obj.info('epochs: %i' % n_epochs)
            self.log_obj.info('neighbourhood_init: %e' % neighbourhood_init)
            self.log_obj.info('neighbourhood_shrink: %e' % neighbourhood_shrink)
            self.log_obj.info('neighbourhood_fin: %e' % neighbourhood_fin)
            self.log_obj.info('alpha_init: %e' % alpha_init)
            self.log_obj.info('alpha_shrink: %e' % alpha_shrink)
            self.log_obj.info('alpha_fin: %e' % alpha_fin)
            self.log_obj.info('stopping_tol: %e' % stopping_tol)

        start = time()
        for epoch in range(n_epochs):
            alpha = alpha_init / (1.0 + epoch * 100 / float(n_epochs))
            neighbourhood = neighbourhood_init / (1.0 + epoch * neighbourhood_init / float(n_epochs))

            if stopping_tol > 0.0:
                temp_W = self.W

            if f is not None:
                f(self, {'epoch':epoch,'alpha':alpha,'neighbourhood':neighbourhood}, *f_arg)

            if self.log_obj is not None: self.log_obj.info('Epoch %i (%i sec), alpha %e, neighbourhood %e' % (epoch, time() - start, alpha, neighbourhood))
            
            np.random.shuffle(X)
            for x in X:
                activations = self.Activations(x)
                minlocation = self.WinnerLoc(activations)
                rates = self.Rates(minlocation, neighbourhood)
                self.W = self.W * (1.0 - alpha * rates) + alpha * rates * x

            if stopping_tol > 0.0:
                avg_loss = np.sum((self.W - temp_W)**2) / reduce(lambda a,b:a*b, self.W.shape)
                if avg_loss < stopping_tol:
                    break
                
        if self.log_obj is not None:
            self.log_obj.info('Training complete (%i sec)' % (time() - start))

    def Save(self, filename):

        with open(filename, 'w') as f:
            f.write(pkl.dumps(self.W))
            
    def __Load(self, loadfile):

        with open(loadfile, 'r') as f:
            return pkl.loads(f.read())



class T_SOM(object):

    def __init__(self, log_obj, shape=None, low=0.0, high=1.0, W=None, loadfile=None):

        self.log_obj = log_obj

        if W is not None:
            self.W = W
        elif shape is not None:
            self.W = np.random.uniform(low=low, high=high, size=shape)
        elif loadfile is not None:
            self.W = self.__Load(loadfile)
        else:
            raise ValueError('Nothing to initialize SOM with.')

        self.W = np.array(self.W)
        
        if log_obj is not None:
            self.log_obj = log_obj
            self.log_obj.info('W has shape: %s' % str(self.W.shape))
        else:
            self.log_obj = None

    def MapEvent(self, x):
        
        activations = np.sum((self.W - x)**2, axis=len(self.W.shape) - 1)
        return np.unravel_index(activations.argmin(),activations.shape)
            

    def Train(self, X, n_epochs=500, alpha_init=1.0, alpha_fin=1e-2, neighbourhood_init=-1,neighbourhood_fin=2.0, f = None, f_arg = tuple()):

        if neighbourhood_init == -1:
            neighbourhood_init = max(self.W.shape[:-1])

        alpha_shrink = float(alpha_init - alpha_fin) / (alpha_fin * n_epochs)
        neighbourhood_shrink = float(neighbourhood_init - neighbourhood_fin) / (neighbourhood_fin * n_epochs)

        if self.log_obj is not None:
            self.log_obj.info('Data size: %i' % len(X))
            self.log_obj.info('Element size: %i' % len(X[0]))
            self.log_obj.info('epochs: %i' % n_epochs)
            self.log_obj.info('neighbourhood_init: %e' % neighbourhood_init)
            self.log_obj.info('neighbourhood_shrink: %e' % neighbourhood_shrink)
            self.log_obj.info('neighbourhood_fin: %e' % neighbourhood_fin)
            self.log_obj.info('alpha_init: %e' % alpha_init)
            self.log_obj.info('alpha_shrink: %e' % alpha_shrink)
            self.log_obj.info('alpha_fin: %e' % alpha_fin)
            self.log_obj.info('Building model...')

        start = time()
        # ********** Build Model **********

        V = T.dvector('V')
        W = T.tensor3('W')
        data = T.dmatrix('data')
        alpha = T.dscalar('alpha')
        neighbourhood = T.dscalar('neighbourhood')

        mgrid = T.mgrid[0:self.W.shape[0], 0:self.W.shape[1]]

        def step(_V, _W, _a, _n, _mgrid):
            activations = T.sum(T.sqr(_W - _V),axis=2)
            winnerloc = T.argmin(activations)
            
            rates = T.shape_padright(T.exp( - (T.sqr(mgrid[1] - (winnerloc %  self.W.shape[0])) + T.sqr(mgrid[0] - (winnerloc //  self.W.shape[0]))) / _n))
            return _W * (1.0 - _a * rates) + _a * rates * _V

        output, updates = theano.scan(fn=step, outputs_info=[W], sequences=[data], non_sequences=[alpha, neighbourhood, mgrid])

        f_step = theano.function(inputs=[W, data, alpha, neighbourhood],outputs=[output[-1]],updates=updates)
        # ********************                                                                                                                                                                        

        if self.log_obj is not None: self.log_obj.info('Model built (%i sec), now training.' % (time() - start))

        for epoch in range(n_epochs):

            a = alpha_init / (1.0 + epoch * alpha_shrink)
            n = neighbourhood_init / (1.0 + epoch * neighbourhood_shrink)

            if f is not None:
                f(self, {'epoch':epoch,'alpha':alpha,'neighbourhood':neighbourhood}, *f_arg)

            if self.log_obj is not None: self.log_obj.info('Epoch %i (%i sec), alpha %e, neighbourhood %e' % (epoch, time() - start, a, n))

            np.random.shuffle(X)
            self.W = f_step(self.W, X, a, n)[0]

        if self.log_obj is not None: self.log_obj.info('Training Complete (%i sec)' % (time() - start))

    def Save(self, filename):

        with open(filename, 'w') as f:
            f.write(pkl.dumps(self.W))

    def __Load(self, loadfile):

        with open(loadfile, 'r') as f:
            return pkl.loads(f.read())
